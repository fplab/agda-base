{-# OPTIONS --without-K #-}
module sets where

open import sets.bool  public renaming (_≟_ to _≟B_)
open import sets.empty public
open import sets.fin   public renaming (_≟_ to _≟F_)
open import sets.nat   public
open import sets.unit  public
open import sets.vec   public
