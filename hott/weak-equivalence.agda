{-# OPTIONS --without-K #-}
module hott.weak-equivalence where

open import hott.weak-equivalence.alternative public
open import hott.weak-equivalence.core public
open import hott.weak-equivalence.properties public
